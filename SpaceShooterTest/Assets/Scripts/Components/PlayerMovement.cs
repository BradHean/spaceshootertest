using Unity.Entities;

[GenerateAuthoringComponent]
public struct PlayerMovement : IComponentData
{
    public float MovementSpeed;
    public float RotationSpeed;
    public Entity Bullet;
}
