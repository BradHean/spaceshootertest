using Unity.Entities;

[GenerateAuthoringComponent]
public struct BasicMovement : IComponentData
{
    public float Speed;
}
