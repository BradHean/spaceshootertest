using Unity.Entities;

[GenerateAuthoringComponent]
public struct InvulnerablePlayerComponent : IComponentData
{
	public Entity PlayerPrefab;
}