using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct Pickup : IComponentData
{
    public ePickupType PickupType;
}
