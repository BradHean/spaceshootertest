using Unity.Entities;

[GenerateAuthoringComponent]
public struct UFOMovementComponent : IComponentData
{
    public float Speed;
    public Entity Bullet;
}
