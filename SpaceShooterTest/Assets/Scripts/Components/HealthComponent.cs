using Unity.Entities;
using UnityEngine;

[GenerateAuthoringComponent]
public struct HealthComponent : IComponentData
{
    public float MaxHealth;
}
