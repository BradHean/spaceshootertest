using Unity.Entities;

[GenerateAuthoringComponent]
public struct CollisionComponent : IComponentData
{
    public float DamageAmount;
    public eSoundType CollisionSound;
    public Entity Prefab;
    public int DestroyedBitsCount;
}
