using Unity.Entities;

[GenerateAuthoringComponent]
public struct SpawnFromEntityComponent : IComponentData
{
	public Entity Prefab;
	public int DestroyedBitsCount;
	public bool IsPlayer;
	public bool IsEnemy;
	public bool IsUFO;
	public bool IsPickup;
	public ePickupType PickupType;
	public int Points;
}
