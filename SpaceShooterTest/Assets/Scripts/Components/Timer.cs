using Unity.Entities;

[GenerateAuthoringComponent]
public struct Timer : IComponentData
{
    // Note that we don't reset the values here since
    // we will remove this component when we're done anyways

    public float ElapsedTime;
    public float TargetDuration;

    public Timer(float targetDuration) : this()
    {
        SetDuration(targetDuration);
    }

    public bool IsDone
    {
        get
        {
            return this.ElapsedTime >= this.TargetDuration;
        }
    }

    private void SetDuration(float targetDuration)
    {
        this.ElapsedTime = 0;
        this.TargetDuration = targetDuration;
    }
}