﻿using Unity.Burst;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using System.Diagnostics;

public class SpawnManager_FromEntity : SystemBase
{
	BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

	protected override void OnCreate()
	{
		m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
	}

	protected override void OnUpdate()
	{
		var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer();
		var rand = new Unity.Mathematics.Random((uint)Stopwatch.GetTimestamp());
		Entities
			.WithName("SpawnManager_FromEntity")
			.WithBurst(FloatMode.Default, FloatPrecision.Standard, true)
			.ForEach((Entity entity, in SpawnFromEntityComponent spawnerFromEntity, in Rotation rotation, in LocalToWorld location) =>
			{
				if (spawnerFromEntity.Prefab != null)
				{
					for (int i = 0; i < spawnerFromEntity.DestroyedBitsCount; i++)
					{
						var instance = commandBuffer.Instantiate(spawnerFromEntity.Prefab);
						commandBuffer.SetComponent(instance, new Translation { Value = location.Position });
						var random = rand.NextInt(0, 360);
						commandBuffer.SetComponent(instance, new Rotation
						{
							Value = math.mul(location.Rotation, quaternion.RotateY(math.radians(random)))
						});
					}
				}
				if (spawnerFromEntity.IsPlayer)
				{
					var type = spawnerFromEntity.PickupType;
					var pos = location.Position;
					var rot = rotation.Value;
					UIManager.RunOnMainThread.Enqueue(() =>
					{
						UIManager.PlayerDied(type == ePickupType.Shield, pos, rot);
					});
				}
				if (spawnerFromEntity.IsPickup)
				{
					var type = spawnerFromEntity.PickupType;
					if(type != ePickupType.Shield)
					{
						UIManager.RunOnMainThread.Enqueue(() =>
						{
							UIManager.PickupCollected(type);
						});
					}
				}
				var points = spawnerFromEntity.Points;
				if (spawnerFromEntity.IsEnemy)
				{
					if (spawnerFromEntity.IsUFO)
					{
						UIManager.RunOnMainThread.Enqueue(() =>
						{
							UIManager.UFODied(points);
						});
					}
					else
					{
						UIManager.RunOnMainThread.Enqueue(() =>
						{
							UIManager.AddScore(points, eSoundType.AsteroidExplodeClip);
						});
					}
					var bits = spawnerFromEntity.DestroyedBitsCount;
					{
						UIManager.RunOnMainThread.Enqueue(() =>
						{
							UIManager.AdjustAsteroidCount(bits - 1);
						});
					}
				}
				commandBuffer.DestroyEntity(entity);
			}).WithoutBurst().Schedule();
		m_EntityCommandBufferSystem.AddJobHandleForProducer(Dependency);
	}
}