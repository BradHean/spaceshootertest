﻿using AOT;
using System;
using System.Collections.Concurrent;
using Unity.Entities;
using UnityEngine;

public class UIManager : MonoBehaviour
{
	public static UIManager Instance;

	private static GameObject playerLifePrefab;
	private static Transform playerLifeTransform;
	private static TMPro.TextMeshProUGUI scoreText;
	private static GameObject startButton;
	private static GameObject inGameUI;

	public static int curentScore;
	public static int livesLeft;
	private static readonly int ufoSpawnPoints = 500;
	private static readonly int pickupSpawnPoints = 750;



	static bool hasUFO;
	static int spawnedUFOCount;
	static bool hasPickup;
	static int spawnedPickupCount;

	private void Awake()
	{
		if (Instance == null)
			Instance = this;
		playerLifePrefab = Resources.Load<GameObject>("PlayerLife");
		playerLifeTransform = GameObject.Find("PlayerLives").transform;
		startButton = Transform.FindObjectOfType<StartButton>(true).gameObject;
		inGameUI = GameObject.Find("InGameUI");
		scoreText = GameObject.Find("ScoreText").GetComponent<TMPro.TextMeshProUGUI>();
		startButton.SetActive(true);
		inGameUI.SetActive(false);
	}

	public static readonly ConcurrentQueue<Action> RunOnMainThread = new ConcurrentQueue<Action>();

	void Update()
	{
		if (!RunOnMainThread.IsEmpty)
		{
			while (RunOnMainThread.TryDequeue(out var action))
			{
				action?.Invoke();
			}
		}
	}

	public void StartGame()
	{
		startButton.SetActive(false);
		inGameUI.SetActive(true);
		curentScore = 0;
		scoreText.text = "0";
		AddLives(GameManager.Instance.StartingPlayerLivesCount);
	}

	static AudioSource ufoSource;
	static AudioSource pickupSource;
	public static void AddScore(int amount, eSoundType sound = eSoundType.None)
	{
		curentScore += amount;
		scoreText.text = curentScore.ToString();
		if ((int)(curentScore / ufoSpawnPoints) > spawnedUFOCount && !hasUFO)
		{
			GameManager.SpawnUFO();
			ufoSource = AudioManager.PlaySound(eSoundType.UFOSoundClip, true, 0.5f);
			hasUFO = true;
			spawnedUFOCount++;
		}
		if ((int)(curentScore / pickupSpawnPoints) > spawnedPickupCount && !hasPickup)
		{
			GameManager.SpawnPickup();
			pickupSource = AudioManager.PlaySound(eSoundType.PickupClip, true);
			hasPickup = true;
			spawnedPickupCount++;
		}
		if (sound != eSoundType.None)
			AudioManager.PlaySound(sound);
	}

	public static void AddLives(int count)
	{
		livesLeft += count;
		AdjustLifeCountObjectsVisibility();
	}

	static float lastPlayerDiedTime;
	static float minPlayerDiedInterval = 0.2f;
	public static void PlayerDied(bool hitShieldPickup, Vector3 pos, Quaternion rot)
	{
		if (hitShieldPickup)
		{
			GameManager.SpawnShieldedPlayer(pos, rot);
		}
		else if (Time.time > lastPlayerDiedTime + minPlayerDiedInterval)
		{
			lastPlayerDiedTime = Time.time;
			AudioManager.PlaySound(eSoundType.PlayerDieClip);
			livesLeft--;
			if (livesLeft < 0)
			{
				livesLeft = 0;
				return;
			}
			else if (livesLeft == 0)
			{
				GameOver();
			}
			else
			{
				AdjustLifeCountObjectsVisibility();
				GameManager.SpawnInvulnerablePlayer();
			}
		}
	}

	public static void UFODied(int amount)
	{
		hasUFO = false;
		AudioManager.StopSource(ufoSource);
		AddScore(amount, eSoundType.AsteroidExplodeClip);
	}

	public static void AdjustAsteroidCount(int amount)
	{
		GameManager.AdjustAsteroidCount(amount);
	}

	public static void PickupCollected(ePickupType type)
	{
		hasPickup = false;
		AudioManager.StopSource(pickupSource);
		GameManager.ProcessPickup(type);
	}

	public static void AdjustLifeCountObjectsVisibility()
	{
		if (livesLeft > playerLifeTransform.childCount)
		{
			while (livesLeft > playerLifeTransform.childCount)
				Instantiate(playerLifePrefab, playerLifeTransform);
		}
		else if (livesLeft <= playerLifeTransform.childCount)
		{
			for (int i = 0; i < playerLifeTransform.childCount; i++)
			{
				var trans = playerLifeTransform.GetChild(i);
				trans.gameObject.SetActive(i <= livesLeft - 1);
			}
		}
	}

	static void GameOver()
	{
		if (ufoSource != null)
			AudioManager.StopSource(ufoSource);
		if (pickupSource)
			AudioManager.StopSource(pickupSource);
		livesLeft = 0;
		GameManager.StopSpawning();
		World.DefaultGameObjectInjectionWorld.EntityManager.DestroyEntity(World.DefaultGameObjectInjectionWorld.EntityManager.UniversalQuery);
		startButton.SetActive(true);
		inGameUI.SetActive(false);
		hasUFO = false;
		hasPickup = false;
		spawnedUFOCount = 0;
		spawnedPickupCount = 0;
		AudioManager.Instance.PlayMusic(true);
	}
}
