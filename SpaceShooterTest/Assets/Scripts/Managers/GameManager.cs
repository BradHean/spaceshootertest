﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using Unity.Entities;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance;

	public static GameObject invulnerablePlayerObject;
	public static GameObject shieldedPlayerObject;
	public static GameObject playerObject;
	public static GameObject UFO;
	private static GameObject[] asteroids;
	private static GameObject[] pickups;

	private static int maxAsteroidCount = 10;
	private static float asteroidSpawnDelay = 2;
	private static int startingPlayerLivesCount = 3;

	private static int currentlySpawnedAsteroidCount;

	public GameObject[] Asteroids => asteroids;

	public int StartingPlayerLivesCount => startingPlayerLivesCount;

	static IEnumerator _asteroidSpawn;
	public static float playerShootDelay = 0.3f;

	private void Awake()
	{
		if (Instance == null)
			Instance = this;
		invulnerablePlayerObject = Resources.Load<GameObject>("InvulnerablePlayer 0");
		shieldedPlayerObject = Resources.Load<GameObject>("ShieldedPlayer");
		playerObject = Resources.Load<GameObject>("Player");
		UFO = Resources.Load<GameObject>("UFO");
		asteroids = Resources.LoadAll<GameObject>("Asteroids");
		pickups = Resources.LoadAll<GameObject>("Pickups");
	}

	public static readonly ConcurrentQueue<Action> RunOnMainThread = new ConcurrentQueue<Action>();

	void Update()
	{
		if (!RunOnMainThread.IsEmpty)
		{
			while (RunOnMainThread.TryDequeue(out var action))
			{
				action?.Invoke();
			}
		}
	}

	public void StartGame()
	{
		UIManager.Instance.StartGame();
		AudioManager.Instance.StartGame();
		StartSpawning();
	}

	public static void SpawnPlayer(Vector3 pos, Quaternion rot)
	{
		Instantiate(playerObject, pos, rot);
	}

	public static void SpawnUFO()
	{
		SpawnGameObject(UFO, isUFO: true, isPickup: false);
	}

	public static void SpawnPickup()
	{
		SpawnGameObject(pickups[UnityEngine.Random.Range(0, pickups.Length)], isUFO: false, isPickup: true);
	}

	static void SpawnGameObject(GameObject obj, bool isUFO, bool isPickup)
	{
		Vector2 spawnPosition = GetSpawnPosition(isUFO, isPickup, out Vector2 targetPosition);
		Vector3 worldSpawnPos = Camera.main.ViewportToWorldPoint(new Vector3(spawnPosition.x, spawnPosition.y, Camera.main.nearClipPlane));
		worldSpawnPos.z = 0;
		Vector3 worldTargetPos = Camera.main.ViewportToWorldPoint(new Vector3(targetPosition.x, targetPosition.y, Camera.main.nearClipPlane));
		worldTargetPos.z = 0;
		var dir = worldTargetPos - worldSpawnPos;
		var enemy = Instantiate(obj, worldSpawnPos, Quaternion.identity);
		enemy.transform.rotation = Quaternion.LookRotation(dir, Vector3.forward);
	}

	public static void SpawnInvulnerablePlayer()
	{
		Instantiate(invulnerablePlayerObject, Vector3.zero, Quaternion.LookRotation(Vector3.up));
	}

	public static void SpawnShieldedPlayer(Vector3 pos, Quaternion rot)
	{
		Instantiate(shieldedPlayerObject, pos, rot);
	}

	public static void StartSpawning()
	{
		SpawnInvulnerablePlayer();
		if (_asteroidSpawn == null)
			_asteroidSpawn = SpawnAsteroids();
		Instance.StartCoroutine(_asteroidSpawn);
	}

	public static void StopSpawning()
	{
		if (_asteroidSpawn != null)
			Instance.StopCoroutine(_asteroidSpawn);
		_asteroidSpawn = null;
		currentlySpawnedAsteroidCount = 0;
	}

	public static void AdjustAsteroidCount(int amount)
	{
		currentlySpawnedAsteroidCount += amount;
	}

	static IEnumerator SpawnAsteroids()
	{
		while (true)
		{
			if (currentlySpawnedAsteroidCount < maxAsteroidCount)
			{
				Vector2 spawnPosition = GetSpawnPosition(false, false, out Vector2 targetPosition);
				Vector3 worldSpawnPos = Camera.main.ViewportToWorldPoint(new Vector3(spawnPosition.x, spawnPosition.y, Camera.main.nearClipPlane));
				worldSpawnPos.z = 0;
				Vector3 worldTargetPos = Camera.main.ViewportToWorldPoint(new Vector3(targetPosition.x, targetPosition.y, Camera.main.nearClipPlane));
				worldTargetPos.z = 0;
				var dir = worldTargetPos - worldSpawnPos;
				var asteroid = Instantiate(asteroids[UnityEngine.Random.Range(0, asteroids.Length)], worldSpawnPos, Quaternion.identity);
				asteroid.transform.rotation = Quaternion.LookRotation(dir, Vector3.forward);
				AdjustAsteroidCount(1);
			}
			yield return new WaitForSeconds(asteroidSpawnDelay);
		}
	}

	static Vector2 GetSpawnPosition(bool isUFO, bool isPickup, out Vector2 targetPosition)
	{
		targetPosition = Vector2.zero;
		float x = 0;
		float y = 0;
		//pick random side of screen to start from
		if(isUFO)
		{
			x = 0;
			y = 0.90f;
		}
		else if(isPickup)
		{
			x = UnityEngine.Random.Range(0.05f, 0.95f);
			y = 1;
		}
		else if (UnityEngine.Random.Range(0, 2) == 1)
		{
			//pick random between left or right side of the viewport
			x = UnityEngine.Random.Range(0, 2) == 1 ? 0 : 1;
			y = UnityEngine.Random.Range(0.01f, 0.999f);
		}
		else
		{
			//pick random between top or bottom of scrthe viewport
			y = UnityEngine.Random.Range(0, 2) == 1 ? 0 : 1;
			x = UnityEngine.Random.Range(0.01f, 0.999f);
		}
		//get a random target position on the opposite side of the viewport
		if (x == 0)
			targetPosition = new Vector2(1, UnityEngine.Random.Range(0.01f, 0.999f));
		else if (x == 1)
			targetPosition = new Vector2(0, UnityEngine.Random.Range(0.01f, 0.999f));
		else if (y == 0)
			targetPosition = new Vector2(UnityEngine.Random.Range(0.01f, 0.999f), 1);
		else if (y == 1)
			targetPosition = new Vector2(isPickup ? x : UnityEngine.Random.Range(0.01f, 0.999f), 0);
		return new Vector2(x, y);
	}

	public void SpawnAsteroidBits(GameObject bits, Vector3 spawnPosition)
	{
		float x = 0;
		float y = 0;
		if (UnityEngine.Random.Range(0, 2) == 1)
		{
			//pick random between left or right side of the viewport
			x = UnityEngine.Random.Range(0, 2) == 1 ? 0 : 1;
			y = UnityEngine.Random.Range(0.01f, 0.999f);
		}
		else
		{
			//pick random between top or bottom of scrthe viewport
			y = UnityEngine.Random.Range(0, 2) == 1 ? 0 : 1;
			x = UnityEngine.Random.Range(0.01f, 0.999f);
		}
		var targetPosition = new Vector2(x, y);
		Vector3 worldSpawnPos = Camera.main.ViewportToWorldPoint(new Vector3(spawnPosition.x, spawnPosition.y, Camera.main.nearClipPlane));
		worldSpawnPos.z = 0;
		Vector3 worldTargetPos = Camera.main.ViewportToWorldPoint(new Vector3(targetPosition.x, targetPosition.y, Camera.main.nearClipPlane));
		var dir = worldTargetPos - worldSpawnPos;
		var asteroid = Instantiate(bits, worldSpawnPos, Quaternion.identity);
		asteroid.transform.rotation = Quaternion.LookRotation(dir, Vector3.forward);
	}

	public static void ProcessPickup(ePickupType type)
	{
		switch (type)
		{
			case ePickupType.FasterBullets:
				GameManager.Instance.StartCoroutine(DoubleFire());
				break;
			case ePickupType.Life:
				UIManager.AddLives(1);
				break;
		}
	}

	static IEnumerator DoubleFire()
	{
		playerShootDelay /= 2;
		yield return new WaitForSeconds(10);
		playerShootDelay *= 2;
	}
}

public enum ePickupType
{
	FasterBullets,
	Life,
	Shield,
	None
}
