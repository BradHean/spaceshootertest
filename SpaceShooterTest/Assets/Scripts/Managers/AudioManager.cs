﻿using System;
using System.Collections.Concurrent;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;

    public AudioSource MusicSource;
    public Transform SFXSourceTransform;
    public static AudioSource[] SFXSources;

    public AudioClip[] Music;
    public AudioClip MenuMusic;

    public static AudioClip PlayerShootClip;
    public static AudioClip PlayerDieClip;
    public static AudioClip AsteroidExplodeClip;
    public static AudioClip UFOSoundClip;
    public static AudioClip PickupClip;
    public static AudioClip TractorBeamClip;
    public static AudioClip UfoShootClip;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        PlayerShootClip = Resources.Load<AudioClip>("PlayerShootClip");
        PlayerDieClip = Resources.Load<AudioClip>("PlayerDieClip");
        AsteroidExplodeClip = Resources.Load<AudioClip>("AsteroidExplodeClip");
        UFOSoundClip = Resources.Load<AudioClip>("UFOSoundClip");
        PickupClip = Resources.Load<AudioClip>("PickupClip");
        TractorBeamClip = Resources.Load<AudioClip>("TractorBeamClip");
        UfoShootClip = Resources.Load<AudioClip>("UfoShootClip");
        SFXSources = SFXSourceTransform.GetComponentsInChildren<AudioSource>();
    }

    public static readonly ConcurrentQueue<Action> RunOnMainThread = new ConcurrentQueue<Action>();

    void Update()
    {
        if (!RunOnMainThread.IsEmpty)
        {
            while (RunOnMainThread.TryDequeue(out var action))
            {
                action?.Invoke();
            }
        }
    }

    private void Start()
    {
        PlayMusic(true);
    }

    public void StartGame()
	{
        PlayMusic(false);
    }

    public void PlayMusic(bool menu)
	{
        if(menu)
            MusicSource.clip = MenuMusic;
        else
            MusicSource.clip = Music[UnityEngine.Random.Range(0, Music.Length)];
        MusicSource.Play();
    }

    public static AudioSource PlaySound(eSoundType type, bool isLooped = false, float volume = 1)
    {
        if (type == eSoundType.None)
            return null;
        var source = GetFreeSource();
        if (source != null)
        {
            source.loop = isLooped;
            AudioClip clip = null;
            switch (type)
            {
                case eSoundType.PlayerShootClip:
                    clip = PlayerShootClip;
                    break;
                case eSoundType.PlayerDieClip:
                    clip = PlayerDieClip;
                    break;
                case eSoundType.AsteroidExplodeClip:
                    clip = AsteroidExplodeClip;
                    break;
                case eSoundType.UFOSoundClip:
                    clip = UFOSoundClip;
                    break;
                case eSoundType.PickupClip:
                    clip = PickupClip;
                    break;
                case eSoundType.TractorBeamClip:
                    clip = TractorBeamClip;
                    break;
                case eSoundType.UfoShootClip:
                    clip = UfoShootClip;
                    break;
            }
            source.volume = volume;
            source.clip = clip;
            source.Play();
        }
        return source;
    }

    static AudioSource GetFreeSource()
    {
        for (int i = 0; i < SFXSources.Length; i++)
        {
            if (!SFXSources[i].isPlaying)
                return SFXSources[i];
        }
        return null;
    }

    public static void StopSource(AudioSource source)
    {
        if (source == null)
            return;
        source.Stop();
        source.loop = false;
        source.clip = null;
    }

    public void PauseAllAudioSources()
    {
        for (int i = 0; i < SFXSources.Length; i++)
        {
            if (SFXSources[i].isPlaying)
                SFXSources[i].Pause();
        }
    }

    public void ResumeAllAudioSources()
    {
        for (int i = 0; i < SFXSources.Length; i++)
        {
            if (SFXSources[i].clip != null)
                SFXSources[i].Play();
        }
    }
}

public enum eSoundType
{
    PlayerShootClip,
    PlayerDieClip,
    AsteroidExplodeClip,
    UFOSoundClip,
    PickupClip,
    TractorBeamClip,
    UfoShootClip,
    UfoWarningClip,
    None
}
