using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

//[AlwaysSynchronizeSystem]
[UpdateAfter(typeof(SpawnManager_FromEntity))]
public class DestroySystem : JobComponentSystem
{
	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
        EntityCommandBuffer commandBuffer = new EntityCommandBuffer(Allocator.TempJob);

        Entities
            .WithAll<DestroyComponent>()
            .ForEach((Entity entity) =>
            {
                commandBuffer.DestroyEntity(entity);
            }).Run();
        commandBuffer.Playback(EntityManager);
        commandBuffer.Dispose();
        return default;
    }
}
