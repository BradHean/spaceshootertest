using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Transforms;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class PlayerMovementSystem : JobComponentSystem
{
	BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;
	PhysicsWorld physicsWorld;

	protected override void OnCreate()
	{
		m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
		physicsWorld = World.DefaultGameObjectInjectionWorld.GetExistingSystem<Unity.Physics.Systems.BuildPhysicsWorld>().PhysicsWorld;
	}

	float lastShootTime;
	quaternion lastRotation;
	float lastHorizontal;
	float lastVertical;

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer();
		float deltaTime = Time.DeltaTime;
		bool rotating = false;
		bool moving = false;
		if (Input.GetKey(KeyCode.UpArrow))
		{
			moving = true;
			lastVertical = Input.GetAxis("Vertical");
		}
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
		{
			rotating = true;
			lastHorizontal = Input.GetAxis("Horizontal");
		}
		bool shoot = false;
		if (Input.GetKey(KeyCode.Space) && Time.ElapsedTime > lastShootTime + GameManager.playerShootDelay)
		{
			lastShootTime = (float)Time.ElapsedTime;
			shoot = true;
		}
		Entities.ForEach((Entity _entity, ref Translation translation, ref Rotation rotation,
			ref PhysicsVelocity _physicsVelocity, ref PhysicsMass _physicsMass, in PlayerMovement movement) =>
		{
			if(moving)
			{
				var speed = Mathf.Min(movement.MovementSpeed * deltaTime * lastVertical, movement.MovementSpeed);
				PhysicsComponentExtensions.ApplyLinearImpulse(ref _physicsVelocity, _physicsMass,
					math.forward(lastRotation) * speed);
			}
			if (rotating)
				rotation.Value = math.mul(rotation.Value, quaternion.RotateY(math.radians(movement.RotationSpeed * lastHorizontal * deltaTime)));
			
			if (!rotating)
			{
				if (moving)
				{
					lastRotation = rotation.Value;
				}
			}

			var pos = Camera.main.WorldToViewportPoint(translation.Value);
			if (pos.x < 0)
				pos.x = 1;
			if (pos.x > 1)
				pos.x = 0;
			if (pos.y < 0)
				pos.y = 1;
			if (pos.y > 1)
				pos.y = 0;
			translation.Value = Camera.main.ViewportToWorldPoint(pos);
			translation.Value.z = 0;
			if (shoot)
			{
				var instance = commandBuffer.Instantiate(movement.Bullet);
				commandBuffer.SetComponent(instance, new Translation { Value = translation.Value });
				commandBuffer.SetComponent(instance, new Rotation
				{
					Value = rotation.Value
				});
				AudioManager.RunOnMainThread.Enqueue(() =>
				{
					AudioManager.PlaySound(eSoundType.PlayerShootClip);
				});
			}
		}).WithoutBurst().Run();
		return default;
	}
}