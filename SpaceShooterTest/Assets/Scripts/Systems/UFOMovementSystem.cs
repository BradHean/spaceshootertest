using System.Diagnostics;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class UFOMovementSystem : JobComponentSystem
{
	BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

	protected override void OnCreate()
	{
		m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
	}

	float lastShootTime;
	float shootDelay = 0.5f;
	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer();
		var rand = new Unity.Mathematics.Random((uint)Stopwatch.GetTimestamp());
		float deltaTime = Time.DeltaTime;
		bool shoot = Time.ElapsedTime > lastShootTime + shootDelay;
		if(shoot)
		{
			lastShootTime = (float)Time.ElapsedTime;
		}
		Entities.ForEach((ref Translation translation, ref Rotation rotation, in LocalToWorld location, 
			in UFOMovementComponent movement) =>
		{
			translation.Value += movement.Speed * deltaTime * math.forward(rotation.Value);
			var pos = Camera.main.WorldToViewportPoint(translation.Value);
			//when the UFO gets to the edge of the screen, move it down slightly for the next pass
			if (pos.x < 0)
			{
				pos.x = 1;
				pos.y -= 0.1f;
			}
			if (pos.x > 1)
			{
				pos.x = 0;
				pos.y -= 0.1f;
			}
			if (pos.y < 0)
				pos.y = 1;
			if (pos.y > 1)
				pos.y = 0;
			translation.Value = Camera.main.ViewportToWorldPoint(pos);
			translation.Value.z = 0;
			if (shoot)
			{
				var instance = commandBuffer.Instantiate(movement.Bullet);
				commandBuffer.SetComponent(instance, new Translation { Value = translation.Value });
				var random = rand.NextInt(0, 360);
				commandBuffer.SetComponent(instance, new Rotation
				{
					Value = math.mul(location.Rotation, quaternion.RotateY(math.radians(random)))
				});
				AudioManager.RunOnMainThread.Enqueue(() => {
					AudioManager.PlaySound(eSoundType.UfoShootClip);
				});
			}
		}).WithoutBurst().Run();
		return default;
	}
}