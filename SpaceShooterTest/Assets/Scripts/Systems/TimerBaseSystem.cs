using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Physics;
using Unity.Transforms;

public class TimerBaseSystem : SystemBase
{
	private EntityQuery timerQuery;
	private EntityCommandBufferSystem entityCommandBufferSystem;

	private float ScaledTime
	{
		get
		{
			return Time.DeltaTime;
		}
	}

	protected override void OnCreate()
	{
		base.OnCreate();
		entityCommandBufferSystem = World.GetOrCreateSystem<EntityCommandBufferSystem>();
		timerQuery = EntityManager.CreateEntityQuery(ComponentType.ReadWrite<Timer>());
	}

	protected override void OnUpdate()
	{
		UpdateTimerJob job = new UpdateTimerJob
		{
			EntityTypeHandle = GetEntityTypeHandle(),
			TimerTypeHandle = GetComponentTypeHandle<Timer>(),
			RotationTypeHandle = GetComponentTypeHandle<Rotation>(),
			LocationTypeHandle = GetComponentTypeHandle<LocalToWorld>(),
			InvulnerableTypeHandle = GetComponentTypeHandle<InvulnerablePlayerComponent>(),
			PhysicsHandle = GetComponentTypeHandle<PhysicsVelocity>(),
			CurrentTimeInterval = ScaledTime,
			CommandBuffer = entityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter()
		};

		Dependency = job.ScheduleParallel(timerQuery, Dependency);
		entityCommandBufferSystem.AddJobHandleForProducer(Dependency);
	}

	[BurstCompile]
	private struct UpdateTimerJob : IJobChunk
	{
		[ReadOnly]
		public EntityTypeHandle EntityTypeHandle;

		[ReadOnly]
		public float CurrentTimeInterval;

		public ComponentTypeHandle<Timer> TimerTypeHandle;
		public ComponentTypeHandle<Rotation> RotationTypeHandle;
		public ComponentTypeHandle<LocalToWorld> LocationTypeHandle;
		public ComponentTypeHandle<InvulnerablePlayerComponent> InvulnerableTypeHandle;
		public ComponentTypeHandle<PhysicsVelocity> PhysicsHandle;
		public EntityCommandBuffer.ParallelWriter CommandBuffer;

		public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
		{
			NativeArray<Entity> entities = chunk.GetNativeArray(EntityTypeHandle);
			NativeArray<Timer> timers = chunk.GetNativeArray(TimerTypeHandle);
			NativeArray<Rotation> rotations = chunk.GetNativeArray(RotationTypeHandle);
			NativeArray<LocalToWorld> locations = chunk.GetNativeArray(LocationTypeHandle);
			NativeArray<InvulnerablePlayerComponent> invulnerables = chunk.GetNativeArray(InvulnerableTypeHandle);
			NativeArray<PhysicsVelocity> velocities = chunk.GetNativeArray(PhysicsHandle);

			for (int i = 0; i < entities.Length; ++i)
			{
				Timer timer = timers[i];
				int sortKey = firstEntityIndex + i;
				timer.ElapsedTime += CurrentTimeInterval;

				if (timer.IsDone)
				{
					Entity instance = CommandBuffer.Instantiate(sortKey, invulnerables[i].PlayerPrefab);
					CommandBuffer.SetComponent(sortKey, instance, new Translation { Value = locations[i].Position });
					CommandBuffer.SetComponent(sortKey, instance, new Rotation { Value = rotations[i].Value });
					CommandBuffer.SetComponent(sortKey, instance, new PhysicsVelocity { Linear = velocities[i].Linear, 
																						Angular = velocities[i].Angular });
					CommandBuffer.DestroyEntity(sortKey, entities[i]);
				}
				timers[i] = timer;
			}
		}
	}
}