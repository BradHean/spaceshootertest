using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[AlwaysSynchronizeSystem]
public class BasicMovementSystem : JobComponentSystem
{
	BeginInitializationEntityCommandBufferSystem m_EntityCommandBufferSystem;

	protected override void OnCreate()
	{
		m_EntityCommandBufferSystem = World.GetOrCreateSystem<BeginInitializationEntityCommandBufferSystem>();
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer();
		float deltaTime = Time.DeltaTime;
		Entities.ForEach((Entity entity, ref Translation translation, ref Rotation rotation, in BasicMovement movement,
			in UFOBulletComponent ufoBullet) =>
		{
			translation.Value += movement.Speed * deltaTime * math.forward(rotation.Value);
			var pos = Camera.main.WorldToViewportPoint(translation.Value);
			if (pos.x <= 0.01f || pos.x >= 0.99f || pos.y <= 0.01f || pos.y >= 0.99f)
				commandBuffer.DestroyEntity(entity);
			else
			{
				translation.Value = Camera.main.ViewportToWorldPoint(pos);
				translation.Value.z = 0;
			}
		}).WithoutBurst().Run();
		Entities.ForEach((Entity entity, ref Translation translation, ref Rotation rotation, in BasicMovement movement, 
			in PlayerBulletComponent playerBullet) =>
		{
 			translation.Value += movement.Speed * deltaTime * math.forward(rotation.Value);
			var pos = Camera.main.WorldToViewportPoint(translation.Value);
			if (pos.x <= 0.01f || pos.x >= 0.99f || pos.y <= 0.01f || pos.y >= 0.99f)
				commandBuffer.DestroyEntity(entity);
			else
			{
				translation.Value = Camera.main.ViewportToWorldPoint(pos);
				translation.Value.z = 0;
			}
		}).WithoutBurst().Run();
		Entities.ForEach((ref Translation translation, ref Rotation rotation, in BasicMovement movement) =>
		{
			translation.Value += movement.Speed * deltaTime * math.forward(rotation.Value);
			var pos = Camera.main.WorldToViewportPoint(translation.Value);
			if (pos.x < 0)
				pos.x = 1;
			if (pos.x > 1)
				pos.x = 0;
			if (pos.y < 0)
				pos.y = 1;
			if (pos.y > 1)
				pos.y = 0;
			translation.Value = Camera.main.ViewportToWorldPoint(pos);
			translation.Value.z = 0;
		}).WithoutBurst().Run();
		return default;
	}
}